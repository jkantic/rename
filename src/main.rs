use std::env;
use std::fs;
use std::path;
use std::u32;
use std::str::FromStr;

/// The main method.
fn main() {

    let path: path::PathBuf;
    let mut counter: u32;
    match parse_args() {
        Ok(x) => {
            match x {
                Some((p, s)) => {
                    path = p;
                    counter = s;
                },
                None => return
            }
        },
        Err(e) => {
            println!("Error: {}", e);
            print_help();
            return;
        }
    }

    // get a list of files in the directory
    let files = match path.read_dir() {
        Ok(x) => x,
        Err(e) => {
            panic!("Error: Ein Fehler ist aufgetreten:\n{}", e);
        }
    };

    // try to rename each file
    for file in files {
        let entry = match file {
            Ok(x) => x,
            Err(e) => {
                println!("Warnung: Konnte eine Datei nicht lesen:\n{}", e);
                continue;
            }
        };

        let p = entry.path();
        if p.is_file() {
            match rename_file(&p, counter) {
                Err(e) => println!("Warnung:\n{}", e),
                _ => counter += 1
            }
        }
    }
}

/// Parses the command line arguments.
fn parse_args() -> Result<Option<(path::PathBuf, u32)>, String> {
    let args : Vec<String> = env::args().collect();
    if args.len() < 2 {
        return Err(String::from("Es wurde kein Verzeichnis spezifiziert!"));
    }

    let mut start_num: u32 = 1;
    let mut rename_path_specified = false;
    let mut rename_path = path::PathBuf::new();
    let mut idx: usize = 1;
    while args.len() > idx {
        let arg = args[idx].clone();
        match arg.as_ref() {
            "-h" => {
                print_help();
                return Ok(None);
            },

            "-n" => {
                idx += 1;
                if args.len() <= idx {
                    return Err(String::from("Es wurde beim Argument -n keine Zahl angegeben!"));
                }
                match u32::from_str(& args[idx]) {
                    Ok(x) => {
                        start_num = x;
                        idx += 1;
                    },
                    Err(_) => return Err(String::from("Die bei der Option -n angegebene Zahl ist ungültig!"))
                }
            },

            _ => {
                if rename_path_specified {
                    return Err(String::from("Es wurden mehrere Pfade spezifiziert!"));
                }
                else {
                    rename_path_specified = true;
                }

                rename_path = path::PathBuf::new();
                rename_path.push(path::Path::new(& args[idx]));
                idx += 1;
            }
        }
    }

    if !rename_path_specified {
        return Err(String::from("Es wurde kein Verzeichnis spezifiziert!"));
    }

    if !rename_path.is_dir() {
        return Err(String::from("Der angegebene Pfad ist kein Verzeichnis!"));
    }

    Ok(Some((rename_path, start_num)))
}

/// Prints help text.
fn print_help() {
    println!("Starte das Programm wie folgt:");
    println!("rename <Pfad-zum-Ordner> <optionale-Argumente>");
    println!("Optionale Argumente:");
    println!("\t-n N\tDie Numerierung startet mit N, wobei N eine positive ganze Zahl sein muss.");
}

/// Renames the file specified by the given path buffer to the specified counter value.
/// The file extension will be retained if it has any.
fn rename_file(p: & path::PathBuf, counter: u32) -> Result<(), String> {

    let ext = match p.extension() {
        Some(x) => x,
        None => std::ffi::OsStr::new("")
    };

    let new_file_path = match p.parent() {
        Some(x) => x,
        None => panic!("Die Datei {} besitzt einen ungültigen Pfad!", p.display())
    };

    let file_name_str = & format!("{}", counter);
    let new_name_path = path::Path::new(file_name_str);
    let new_file_path = new_file_path.join(new_name_path.with_extension(ext));

    match fs::rename(p, & new_file_path) {
        Ok(_) => {
            println!("Bennene {} in {} um.", p.display(), new_file_path.display());
            Ok(())
        },
        Err(_) => Err(format!("Konnte folgende Datei nicht umbenennen: {}", p.display()))
    }
}